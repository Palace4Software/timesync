#!/usr/bin/env python3
### Setup language for timesync ###

def main(config_path, langpacks, firststart=False):
    print("Initalize 'Change lanuguage' subwindow...")
    import tkinter as gui
    from tkinter import messagebox
    import os

    def setlanguage(language, langpacks, firststart):
        print(language)
        with open(config_path + "/language.cfg", "w") as langcfg:
            print(language, file=langcfg)
            print("", file=langcfg)
        print("Language set to " + language + ".")
        GUI.destroy()
        languageID = -1
        while True: # get languageID for messagebox-text
            languageID = languageID + 1
            if language == langpacks[languageID][0]:
                break
        if firststart == True:
            messagebox.showwarning(langpacks[languageID][32], langpacks[languageID][33] + " " + language)
        else:
            messagebox.showwarning(langpacks[languageID][32], langpacks[languageID][33] + " " + language + "\n\n" + langpacks[languageID][34])
    
    GUI = gui.Tk()
    GUI.title("Choose language")
    GUI.minsize(width="200", height="150")
    
    headline = gui.Label(GUI, text="Choose language", font=("Liberation Sans", 13, "bold")).pack(expand=True, fill="both", pady=10)

    for i in range(len(langpacks)):
        tmp_langpacks = langpacks[i][0]
        gui.Button(GUI, text=(tmp_langpacks), command=lambda lamb=tmp_langpacks: setlanguage(lamb, langpacks, firststart)).pack(expand=False, fill="x") 
    
    spaceholder = gui.Label(GUI).pack(expand=True, fill="both")
    
    print("Initialisation of subwindow finished.")
    
    GUI.mainloop()


### for debugging: start timesync.py and use the "Settings" button, then use the "Change language" button.
