#!/usr/bin/env python3
### Settings for timesync ###

def main(config_path, cache_path, currentlanguage, langpacks):
    print("Initalize 'Settings' subwindow...")
    import tkinter as gui
    from tkinter import messagebox
    import subprocess
    import requests
    import time
    import components.languagesetup as languagesetup # import local file "languagesetup.py"
    
    
    def configerror():
        messagebox.showerror("Critical config Error", "The application has recognized an problem with the configuration-files.")
        print("Critical config Error - The application has recognized an problem with the configuration-files.\nPlease report the issue at 'https://codeberg.org/Palace4Software/timesync/issues'")
        quit()
    
    def readsyncconfig(): # load current autosync configuration
        print("Load current AutoSync configuration...")
        autosyncconfig = open("/etc/timesync-autosync.cfg", "r")
        autosyncconfig = autosyncconfig.readlines()
        for i in range(len(autosyncconfig)):
            autosyncconfig[i] = autosyncconfig[i].replace("\n", "")
        autosyncconfig[0] = autosyncconfig[0].replace("#", "")
        # change string "True"/"False" to boolean True/False
        if autosyncconfig[0] == "True": autosyncconfig[0] = eval(autosyncconfig[0])
        elif autosyncconfig[0] == "False": autosyncconfig[0] = eval(autosyncconfig[0])
        else: configerror()
        if autosyncconfig[2] == "True": autosyncconfig[2] = eval(autosyncconfig[2])
        elif autosyncconfig[2] == "False": autosyncconfig[2] = eval(autosyncconfig[2])
        else: configerror()
        return autosyncconfig
    
    def readupdateconfig(): # read autoupdate-config
        print("Load current AutoUpdate configuration...")
        autoupdateconfig = open(config_path + "/autoupdate.cfg", "r")
        autoupdateconfig = autoupdateconfig.readline().replace("\n", "")
        if autoupdateconfig == "True" or autoupdateconfig == "False": autoupdateconfig = eval(autoupdateconfig) # change string "True"/"False" to boolean True/False
        else: configerror()
        return autoupdateconfig
    
    autosyncconfig = readsyncconfig() # read autosync-config at start of settings
    global autoupdateconfig # need to be global!
    autoupdateconfig = readupdateconfig() # read autoupdate-config at start of settings
    
    #other definitions
    def languagesubwindow(*args):
        GUI.destroy()
        print("Start languagesetup.")
        languagesetup.main(config_path, langpacks)
    
    def changeautoupdate():
        global autoupdateconfig # import global variable
        autoupdateconfig = not autoupdateconfig
        with open(config_path + "/autoupdate.cfg", "w") as updatecfg:
            print(str(autoupdateconfig), file=updatecfg)
            print("", file=updatecfg)
        if autoupdateconfig == True:
            print("Automatic update-check enabled.")
        else:
            print("Automatic update-check disabled.")
    
    def infotextcheck():
        if autosyncconfig[2] == False:
            infotext.configure(text=(currentlanguage[42] + "\n" + currentlanguage[43])) # sync every restart once
        elif autosyncconfig[2] == True:
            infotext.configure(text=(currentlanguage[42] + "\n" + currentlanguage[44])) # sync every restart and every 6h
        else:
            configerror()
    
    def mvtoetc():
        time.sleep(0.5) # necessary to prevent errors in Debian Linux (bookworm, Gnome) under x11
        check = subprocess.run(f"pkexec cp {cache_path}/tmp-timesync-autosync.cfg /etc/timesync-autosync.cfg && rm {cache_path}/tmp-timesync-autosync.cfg", shell=True)
        if check.returncode == 0:
            # if True, set to False or the other way round
            # "autosyncconfig[0] = not autosyncconfig[0]" isn't possible, because if you change the webadress, while it's already enabled, it would be False
            tmpread = readsyncconfig() # re-read config
            if tmpread[0] == False:
                autosyncconfig[0] = False
            elif tmpread [0] == True:
                autosyncconfig[0] = True
            print("Config succesful saved.")
            showinfo(currentlanguage[28], currentlanguage[54]) # successful - config saved
            error = False
            return error
        else:
            print("Problem while saving the config.")
            showerror(currentlanguage[30], currentlanguage[37]) # error - timesync wasn't able to save config
            error = True
            return error
    
    def autosync():
        if syncadressline.winfo_ismapped() == True:
            print("AutoSync Checkbutton disabled - Hide entryline and savebutton...")
            syncadressline.pack_forget()
            regularlysync.pack_forget()
            savebutton.pack_forget()
            infotext.pack_forget()
            if not autosyncconfig[0] == False: # if autosync is not yet switched off, switch it off
                print("AutoSync disabled - Saving config...")
                syncadress = syncadressline.get()
                with open(cache_path + "/tmp-timesync-autosync.cfg", "w") as autosynccfg:
                    print("#False", file=autosynccfg)
                    if syncadress == currentlanguage[9]: # if webadress equals default entry, write "Enter webadress"
                        print("Enter webadress", file=autosynccfg)
                    else: # else write content of entryfield
                        print(syncadress, file=autosynccfg)
                    print(str(autosyncconfig[2]), file=autosynccfg)
                    print("", file=autosynccfg)
                if mvtoetc() == True: # if an error occurred while saving, undo "pack_forget" changes
                    print("Error while saving! Undo changes...")
                    packentryline(False)
                    autosync.select()
        else:
            packentryline()
    
    def packentryline(showdebug=True): #this have to be a own module, because multiuse (see: below)  |  optional argument given, default: True
        if showdebug == True: print("AutoSync Checkbutton enabled - Show entryline and savebutton...")
        endspace.pack_forget()
        syncadressline.pack(expand=False, fill="x")
        regularlysync.pack()
        savebutton.pack()
        infotextcheck() # configure infotext
        infotext.pack(expand=False, fill="none", ipady=10)
        endspace.pack(expand=True, fill="both")
    
    def save():
        #happens if savebutton is pressed
        print("AutoSync enabled - Saving config...")
        if syncadressline.winfo_ismapped() == True:
            syncadress = syncadressline.get()
            if not syncadress == currentlanguage[9]:
                #checks for "http(s)://" in the webadress and remove it
                if syncadress[:7] == "http://" or syncadress[:8] == "https://":
                    syncadress = syncadress.removeprefix("http://").removeprefix("https://")
                #check if website exist (or internet connection is etablished)
                print("Checking the connection to the website...")
                try:
                    requests.get("http://" + syncadress)
                    cancontinue = True
                except:
                    cancontinue = False
                #if website exist - save; if not - ask "are you sure, that you want to save"
                if cancontinue == True: # website exist
                    print("Website exist. Saving...")
                    with open(cache_path + "/tmp-timesync-autosync.cfg", "w") as autosynccfg:
                        print("#True", file=autosynccfg)
                        print(syncadress, file=autosynccfg)
                        print(str(autosyncconfig[2]), file=autosynccfg)
                        print("", file=autosynccfg)
                    mvtoetc()
                else: # website does not exist
                    print("Website could not be found.")
                    if askyesno(currentlanguage[46], str("http://" + syncadress) + "\n\n" + currentlanguage[47]) == True: # format error - are you sure you want to save?
                        #force save (website does not exist, but user still want to save)
                        print("Force save! User allowed saving.")
                        showwarning(currentlanguage[48], currentlanguage[49]) # Warning - time will just sync, if website is accessible
                        with open(cache_path + "/tmp-timesync-autosync.cfg", "w") as autosynccfg:
                            print("#True", file=autosynccfg)
                            print(syncadress, file=autosynccfg)
                            print(str(autosyncconfig[2]), file=autosynccfg)
                            print("", file=autosynccfg)
                        mvtoetc()
                    else:
                        #do not save (cancel)
                        print("User canceled saving, because website does not exist (or user do not have internet access)!")
            else:
                print("Saving canceled - Format error.")
                showerror(currentlanguage[35], currentlanguage[36]) # format error - enter valid webadress
        else:
            showerror("Internal Error", "The application has recognized an internal error.")
            print("Internal Error - The application has recognized an internal error.\nPlease report the issue at 'https://codeberg.org/Palace4Software/timesync/issues'")
    
    def changeregularlysync():
        autosyncconfig[2] = not autosyncconfig[2]
        infotextcheck()
    
    def showerror(title, message):
        # hide settings-window, if error-message appears
        GUI.withdraw()
        messagebox.showerror(title, message)
        GUI.deiconify()
    
    def showwarning(title, message):
        # hide settings-window, if warning-message appears
        GUI.withdraw()
        messagebox.showwarning(title, message)
        GUI.deiconify()
    
    def showinfo(title, message):
        # hide settings-window, if error-message appears
        GUI.withdraw()
        messagebox.showinfo(title, message)
        GUI.deiconify()
    
    def askyesno(title, message):
        # hide settings-window, if ask-message appears
        GUI.withdraw()
        asktest = messagebox.askyesno(title, message)
        GUI.deiconify()
        return asktest
    
    #create GUI
    GUI = gui.Tk()
    GUI.title(currentlanguage[12])
    GUI.minsize(width="250", height="200")
    
    headline = gui.Label(GUI, text=currentlanguage[12], font=("Liberation Sans", 13, "bold")).pack(expand=True, fill="both", pady=15)
    
    languagebutton = gui.Button(GUI, text=currentlanguage[38], command=languagesubwindow) # change language
    languagebutton.pack(pady=10)
    
    autoupdate = gui.Checkbutton(GUI, text=currentlanguage[57], command=changeautoupdate) # Enable automatic update check
    autoupdate.pack(expand=False, fill="x", pady=10)
    
    autosync = gui.Checkbutton(GUI, text=currentlanguage[39], command=autosync) # AutoSync
    autosync.pack(expand=False, fill="x", pady=5)
    
    endspace = gui.Label(GUI)
    endspace.pack(expand=True, fill="both")
    
    ## load dummys
    print("Load dummys...")
    #syncadressline - input line
    syncadressline = gui.Entry(GUI)
    if autosyncconfig[1] == "Enter webadress":
        syncadressline.insert(0, currentlanguage[9]) # insert "Enter webadress" in right localisation
    else:
        syncadressline.insert(0, autosyncconfig[1]) # insert last used webadress from config
    #regularly sync checkbox
    regularlysync = gui.Checkbutton(GUI, text=currentlanguage[41], command=changeregularlysync) # Sync every 6 hours
    #savebutton
    savebutton = gui.Button(GUI, text=currentlanguage[40], command=save) # Save
    #infotext
    infotext = gui.Label(GUI, font=("Liberation Sans Narrow", 10))
    
    
    #check if autoupdate is enabled and preset the mark in the Checkbutton
    if autoupdateconfig == True:
        autoupdate.select()
    else:
        autoupdate.deselect()
    #check if autosync is enabled and preset the mark in the Checkbutton
    if autosyncconfig[0] == True:
        autosync.select()
        packentryline()
    else:
        autosync.deselect()
    #check if regularlysync is enabled and preset the mark in the Checkbutton
    if autosyncconfig[2] == True:
        regularlysync.select()
    else:
        regularlysync.deselect()
    
    print("Initialisation of subwindow finished.")
    
    GUI.mainloop()


### for debugging: start timesync.py and use the "Settings" button.
