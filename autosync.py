#!/usr/bin/env python3
### AutoSync systemd-service application for timesync ###

#import
import time
import subprocess
import requests

print("timesync-autosync - systemd-service started!", flush=True) # flush shows the debugging message right away and doesn't wait for the time limit to end

#read config
print("Read user config...", flush=True)
autosyncconfig = open("/etc/timesync-autosync.cfg", "r")
autosyncconfig = autosyncconfig.readlines()
for i in range(len(autosyncconfig)):
    autosyncconfig[i] = autosyncconfig[i].replace("\n", "")
autosyncconfig[0] = autosyncconfig[0].replace("#", "")
if autosyncconfig[0] == "True": autosyncconfig[0] = eval(autosyncconfig[0])
elif autosyncconfig[0] == "False": autosyncconfig[0] = eval(autosyncconfig[0])
else: exit(1)
if autosyncconfig[2] == "True": autosyncconfig[2] = eval(autosyncconfig[2])
elif autosyncconfig[2] == "False": autosyncconfig[2] = eval(autosyncconfig[2])
else: exit(1)

def syncnow():
    #tests for internet connection
    print("Test for internet connection...", flush=True)
    try:
        requests.get("http://" + autosyncconfig[1])
        cancontinue = True
    except:
        cancontinue = False
    
    #sync, if possible
    if cancontinue == True:
        print("Sync...", flush=True)
        subprocess.run(f'''date -s "$(wget --method=HEAD -qSO- --max-redirect=0 {autosyncconfig[1]} 2>&1 | sed -n 's/^ *Date: *//p')"''', shell=True)
    else:
        print("Sync canceled: No internet connection or website does not exist!")

#service
if autosyncconfig[0] == False:
    print("AutoSync is not enabled. Exit service...", flush=True)
elif autosyncconfig[0] == True:
    print("AutoSync is enabled. Wait 60s until sync...", flush=True)
    time.sleep(60)
    syncnow()
    if autosyncconfig[2] == True:
        while True:
            lefthours = 6
            for i in range(6):
                print(f"Wait {str(lefthours)}h until next sync...", flush=True)
                time.sleep(3600)
                lefthours = lefthours - 1
            syncnow()
    print("Exit service...", flush=True)
exit()
