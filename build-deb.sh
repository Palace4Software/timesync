#!/bin/bash

### Automatic .deb creator for timesync ###

version=$(cat version.txt)

# move files to deb building folder
mkdir -p deb/usr/lib/timesync/images/
mkdir -p deb/usr/lib/timesync/langpack/
mkdir -p deb/usr/lib/timesync/components/
cp timesync.py deb/usr/lib/timesync/timesync
cp autosync.py deb/usr/lib/timesync/autosync
cp components/* deb/usr/lib/timesync/components/
cp images/icon.png deb/usr/lib/timesync/images/
cp version.txt deb/usr/lib/timesync/
ln -s /usr/share/doc/timesync/copyright deb/usr/lib/timesync/LICENSE
cp NOTICE deb/usr/lib/timesync/
cp screenshot.png deb/usr/lib/timesync/
cp README.md deb/usr/lib/timesync/
cp langpack/* deb/usr/lib/timesync/langpack/

mkdir -p deb/usr/share/doc/timesync/
ln -s /usr/share/common-licenses/Apache-2.0 deb/usr/share/doc/timesync/copyright

mkdir -p deb/usr/share/icons/hicolor/256x256/apps/
ln -s /usr/lib/timesync/images/icon.png deb/usr/share/icons/hicolor/256x256/apps/timesync.png

mkdir -p deb/usr/share/applications/
cp timesync.desktop deb/usr/share/applications/

# chmod
chmod +x deb/DEBIAN/postinst
chmod +x deb/DEBIAN/prerm

chmod +x deb/usr/lib/timesync/timesync
chmod +x deb/usr/lib/timesync/autosync
chmod +x deb/usr/bin/timesync

# build
dpkg-deb --build -Zxz --root-owner-group deb
mkdir installer
mv deb.deb installer/timesync-$version.deb

# cleanup
rm -r deb/usr/lib
rm -r deb/usr/share/
chmod -x deb/usr/bin/timesync
