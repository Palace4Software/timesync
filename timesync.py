#!/usr/bin/env python3
### Date and Time Syncronization program ###

#Import
print("Import functions...")
import os
import argparse
import tkinter as gui
from tkinter import messagebox
from tkinter import PhotoImage
import subprocess
import threading as libmultitask
import platformdirs
import components.languagesetup as languagesetup # import local file "languagesetup.py"
import components.settings as settings # import local file "settings.py"
import webbrowser
import requests
print("Import finished.")

# internalerror string
interr = str("INTERNAL ERROR: The application has recognized an internal error.\nPlease report the issue at 'https://codeberg.org/Palace4Software/timesync/issues'")

#Get run_path, cache-folder and config-folder (create config-folder if not exist)
run_path = os.path.abspath(os.path.dirname(__file__)) # get directory of current file (without os.path.dirname it would be just the current file)

langpackfolder = (os.path.abspath(os.path.dirname(__file__)) + "/langpack")

cache_path = str(platformdirs.user_cache_dir())

config_path = platformdirs.user_config_dir("timesync")
configexist = os.path.isdir(config_path)
if not configexist == True:
    os.makedirs(config_path)

#Set version (show it in debugging mode)
print("Application is running in", run_path)
print("Configuration will be saved in", config_path)
version = open(run_path + "/version.txt", "r")
version = version.readline().replace("\n", "")
print("timesync version", version)



print("Checking for config-errors, because of updates...")
#set dummy-variables, otherwise the application would crash on "#clear variables"
language = ""
configexist = ""
command = ""

#remove whole config folder, if update from version 2.1.0 or earlier
configexist = os.path.isfile(config_path + "/language.cfg")
if not configexist == True: # needed, because config cannot be read, if file do not exist
    with open(config_path + "/language.cfg", "w") as langcfg:
        print("None", file=langcfg) # create language.cfg if not exist and write "None" in it
        print("", file=langcfg)
language = open(config_path + "/language.cfg", "r")
language = language.readline()
language = language.replace("\n", "") # remove empty line(s) from the variable, because "if language == [...]" does not work otherwise

if language == "DE" or language == "EN":
    command = str('''rm -r ''' + config_path)
    subprocess.run(command, shell=True)
    os.makedirs(config_path)

#replace ＃简体中文 with 简体中文 for chinese language (version 2.2.0)
configexist = os.path.isfile(config_path + "/language.cfg")
if configexist == True:
    language = open(config_path + "/language.cfg", "r")
    language = language.readline()
    language = language.replace("\n", "")
    if language == "＃简体中文":
        with open(config_path + "/language.cfg", "w") as langcfg:
            print("简体中文", file=langcfg)
            print("", file=langcfg)

#remove firststart.cfg, if update from version 2.4.2 or earlier
configexist = os.path.isfile(config_path + "/firststart.cfg")
if configexist == True:
    os.remove(config_path + "/firststart.cfg")

#clear variables
del language
del configexist
del command



def updatecheck(): # also needed for argument "--update", which is the reason why it is here and not through fullupgradecheck
    #test for internet-connection
    try:
        requests.get("https://codeberg.org/Palace4Software/timesync/raw/branch/main/version.txt")
        cancontinue = True
    except:
        cancontinue = False
    #check for updates
    if cancontinue == True:
        command = str(f'''wget -q https://codeberg.org/Palace4Software/timesync/raw/branch/main/version.txt -P {cache_path}/ && mv {cache_path}/version.txt {cache_path}/timesync-vers.tmp''')
        successfulchecker = subprocess.run(command, shell=True)
        if successfulchecker.returncode == 0:
            update = open(cache_path + "/timesync-vers.tmp", "r")
            update = update.readline().replace("\n", "")
            subprocess.run(str(f'''rm {cache_path}/timesync-vers.tmp'''), shell=True)
        else:
            update = "internalerror"
    else:
        update = "no-connection-error"
    return update


#argparse - Attributes/Arguments
print("Test for attributs...")
attributes = argparse.ArgumentParser() # prepair argparse
#define all arguments
attributes.add_argument("--sync", action="store", type=str, help="Sync time with custom server SYNC   (e.g. --sync debian.org)")
attributes.add_argument("--gui", action="store_true", help="Start graphical user interface")
attributes.add_argument("--update", action="store_true", help="Search for updates")

attribute = attributes.parse_args() # apply

if attribute.sync: # if argument "--sync" is given
    print("\n\nTimesync - Console mode.")
    syncadress = str(attribute.sync) #set syncadress to variable
    #checks for "http(s)://" in the webadress and remove it
    if not syncadress[:7] == "http://" and not syncadress[:8] == "https://":
        syncadress = ("http://" + syncadress)
    #checks for internet connection or website exist
    try:
        requests.get(syncadress)
        cancontinue = True
    except:
        cancontinue = False
    #if internet connection exists - sync; else - sync not
    if cancontinue == True:
        print("Sync with " + syncadress + " ...")
        command = (f'''sudo date -s "$(wget --method=HEAD -qSO- --max-redirect=0 {syncadress} 2>&1 | sed -n 's/^ *Date: *//p')"''')
        successfulchecker = subprocess.run((command), shell=True)
        #check if sync was successful
        if successfulchecker.returncode == 0:
            print("The query was succesful")
        else:
            print("Error - Wrong password or process aborted")
            exit(1)
    else:
        print("Error - No internet connection or website does not exist: " + syncadress)
        exit(1)
    exit()
elif attribute.update:
    print("Check for updates...")
    update = updatecheck()
    if not update == "internalerror":
        if not update == "no-connection-error":
            if version < update:
                print("Update available!")
                while True: # repeat question until user answered with "Y" or "N"
                    installyn = input("Do you want to install the update? [Y/n] ")
                    if not installyn.strip(): installyn = "Y" # set installyn to "Y", if user enters nothing
                    installyn = installyn.upper()
                    if installyn == "Y" or installyn == "N":
                        # set installyn to boolean
                        if installyn == "Y": installyn = True
                        else: installyn = False
                        # exit question-loop
                        break
                if installyn == True:
                    #check connection to update-package
                    debdownload = (f"https://codeberg.org/Palace4Software/timesync/releases/download/v{update}/timesync-{update}.deb")
                    try:
                        requests.get(debdownload)
                        subprocess.run(f'''wget -q {debdownload} -P {cache_path}/''', shell=True)
                        cancontinue = True
                    except:
                        cancontinue = False
                    if cancontinue == True: # perform update
                        if not subprocess.run(f'''sudo apt install {cache_path}/timesync-{update}.deb -y''', shell=True).returncode == 0:
                            print("ERROR: Wrong password.")
                            exit(1)
                        else:
                            print("Update successfully installed!")
                        if not subprocess.run(f'''rm {cache_path}/timesync-*.deb*''', shell=True).returncode == 0:
                            print("WARNING: Error while removing cache files!")
                    else: # unknown connection error
                        print("ERROR: Unknown (connection) error! Check internet connection!")
                        exit(1)
                else:
                    print("Update will not be installed.")
            else:
                print("Currently are no updates available.")
        else:
            print("Error - Couldn't check for updates! Check your internet connection.")
            exit(1)
    else:
        print(interr)
        exit(1)
    exit()
elif attribute.gui:
    print("Timesync - Graphical mode.")
else:
    print("\n\n")
    attributes.print_help()
    print("\n\nNo argument given.\nExit application...")
    exit()


#language
print("Load language packs...")

installedlanguages = os.listdir(langpackfolder)
print("Found following langpacks:", installedlanguages, "\nGet langpack entrys...")
for i in range(len(installedlanguages)):
    #set count to "which list entry" (means which language)
    count = i
    #read language files
    installedlanguages[count] = open(langpackfolder + "/" + installedlanguages[count], "r") # read current .langpack file
    installedlanguages[count] = installedlanguages[count].readlines() # set current .langpack file to list
    for a in range(len(installedlanguages[count])): #remove "\n" (newline) from the list
        installedlanguages[count][a] = installedlanguages[count][a].replace("\n", "")
    installedlanguages[count][0] = installedlanguages[count][0].replace("#", "") # remove the "#" from the first line

print("Testing for language...")

def lanuguageconfig(): # function to load language-config
    configexist = os.path.isfile(config_path + "/language.cfg")
    if not configexist == True:
        with open(config_path + "/language.cfg", "w") as langcfg:
            print("None", file=langcfg) # create language.cfg if not exist and write "None" in it
            print("", file=langcfg)
    language = open(config_path + "/language.cfg", "r")
    language = language.readline()
    language = language.replace("\n", "") # remove empty line(s) from the variable, because "if language == [...]" does not work otherwise
    return language

language = lanuguageconfig() # initial load of language-config

if language == "None":
    languagesetup.main(config_path, installedlanguages, firststart=True)
    language = lanuguageconfig() # if firststart (means that "None" is written in config-file), load language-config second time, because otherwise errors

#get needed language
languageID = -1
while True:
    languageID = languageID + 1
    if language == installedlanguages[languageID][0]:
        break # if the language match the config: exit the loop and continue
print("Following language has been loaded:", installedlanguages[languageID][0])


#autoupdate
configexist = os.path.isfile(config_path + "/autoupdate.cfg")
if not configexist == True:
    with open(config_path + "/autoupdate.cfg", "w") as updatecfg:
        print("True", file=updatecfg)
        print("", file=updatecfg)
autoupdate = open(config_path + "/autoupdate.cfg", "r")
autoupdate = autoupdate.readline().replace("\n", "")

# change string "True"/"False" to boolean True/False
if autoupdate == "True" or autoupdate == "False":
    autoupdate = eval(autoupdate)


#general commands
print("Load general commands...")
def internalerror():
    # shouldn't ever appear, that's the reason, why I called it internal error
    showerror("Internal Error", "The application has recognized an internal error.")
    print(interr)

def showinfo(title, message, hidegui=True):
    if hidegui == True:
        # hide main-window, if info-message appears
        GUI.withdraw()
        messagebox.showinfo(title, message)
        GUI.deiconify()
    elif hidegui == False:
        #do not hide main-window
        messagebox.showinfo(title, message)
    else:
        internalerror()

def showerror(title, message, hidegui=True):
    if hidegui == True:
        # hide main-window, if error-message appears
        GUI.withdraw()
        messagebox.showerror(title, message)
        GUI.deiconify()
    elif hidegui == False:
        #do not hide main-window
        messagebox.showerror(title, message)
    else:
        internalerror()

def askyesnocancel(title, message, hidegui=True):
    if hidegui == True:
        # hide main-window, if ask-message appears
        GUI.withdraw()
        asktest = messagebox.askyesnocancel(title, message)
        GUI.deiconify()
    elif hidegui == False:
        #do not hide main-window
        asktest = messagebox.askyesnocancel(title, message)
    else:
        answer = "ERROR"
        internalerror()
    #return answer
    return asktest

def aboutprogram(back, update=False):
    if back == False:
        #forget everything
        print("Prepair About screen...")
        spaceholder3.config(text="")
        imageline.pack_forget()
        button1.pack_forget()
        button2.pack_forget()
        button3.pack_forget()
        button4.pack_forget()
        button5.pack_forget()
        settingsbutton.pack_forget()
        aboutbutton.pack_forget()
        exitbutton.pack_forget()
        spaceholder3.pack_forget()
        GUI.title(installedlanguages[languageID][1] + " - " + installedlanguages[languageID][11]) # add "About" to title
        #version and check for updates (timesync-version-section)
        abouttextversion.configure(text=f"{installedlanguages[languageID][15]} {version}\n", font=("Liberation Sans Narrow", 12)) # Version: x.x.x
        abouttextversion.pack()
        aboutbutton.configure(text=(installedlanguages[languageID][18]), command=fullupdatecheck) # "Check for updates"
        aboutbutton.pack()
        if update == True:
            aboutbutton.config(text=(installedlanguages[languageID][55])) # "Download and apply update"
        elif update == False:
            pass
        else:
            internalerror()
        tempspace.pack(expand=True, fill="both")
        #developed by and license (timesync-license-section)
        abouttextlicense.configure(text=f"\n{installedlanguages[languageID][14]} \n\n {installedlanguages[languageID][16]}", font=("Liberation Sans Narrow", 12)) # "developed by Palace4Software; distributed under Apache License"
        abouttextlicense.pack()
        licensebutton.configure(text=(installedlanguages[languageID][17]), font=("Liberation Sans Narrow", 12), fg="#0000ff", cursor="hand2", command=(
            lambda: openweb(run_path + "/LICENSE") # "Click here to show license"
        ))
        licensebutton.pack()
        noticebutton.configure(text=(installedlanguages[languageID][56]), font=("Liberation Sans Narrow", 12), fg="#0000ff", cursor="hand2", command=(
            lambda: openweb(run_path + "/NOTICE") # "Click here to show license-NOTICE file"
        ))
        noticebutton.pack()
        spaceholder3.pack(expand=True, fill="both")
        #back button
        backbutton.configure(text=("←  " + installedlanguages[languageID][19]), command=lambda: aboutprogram(True)) # "Back"
        backbutton.pack(side="left")
        print("About screen ready.")
    elif back == True:
        print("Remove About screen...")
        abouttextversion.pack_forget()
        abouttextlicense.pack_forget()
        licensebutton.pack_forget()
        noticebutton.pack_forget()
        aboutbutton.pack_forget()
        backbutton.pack_forget()
        tempspace.pack_forget()
        spaceholder3.pack_forget()
        imageline.pack(pady=15)
        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()
        spaceholder2.pack_forget()
        spaceholder2.pack(expand=True, fill="both")
        aboutbutton.configure(text=(installedlanguages[languageID][11]), command=lambda: aboutprogram(False)) # restore "About" on About-Button
        GUI.title(installedlanguages[languageID][1]) # restore title: "Date/Time Synchronizer"
        aboutbutton.pack(side="left")
        exitbutton.pack(side="right")
        settingsbutton.pack(side="top")
        spaceholder3.configure(text="")
        print("About screen removed.")
    else:
        internalerror()

def exitprogram():
    # print("Exit program...") # don't have to be here anymore, because after GUI.mainloop() is already the print command
    GUI.destroy()

def fullupdatecheck():
    print("Check for updates...")
    #updatesearch window
    global destroyvar # globalise "destroyvar", so window can be destroyed at the right time
    destroyvar = False
    def updatesearchwindow(kill=False):
        global GUI2 # globalise "GUI2", so window can be destroyed
        if kill == False:
            GUI2 = gui.Tk()
            GUI2.title(installedlanguages[languageID][22]) # "Updater"
            GUI2.minsize(width=165, height=100)
            GUI2.resizable(width=False, height=False)
            gui.Label(GUI2).pack(expand=True, fill="both")
            gui.Label(GUI2, text=(f"{installedlanguages[languageID][20]} \n\n {installedlanguages[languageID][21]}"), font=("Liberation Mono", 12, "italic")).pack(expand=True, fill="both") # Please wait... Timesync checks for updates...
            gui.Label(GUI2).pack(expand=True, fill="both")
            GUI2.after(100, lambda: updatesearchwindow(kill=True)) # start checking if window can be destroyed
            GUI2.mainloop()
        elif kill == True:
            #check every 100ms for destroyvar == True ;  if destroyvar == True: destroy window, else: check again
            if destroyvar == True:
                GUI2.destroy()
                del GUI2
            else:
                GUI2.after(100, lambda: updatesearchwindow(kill=True))
        else:
            internalerror()
    GUI.withdraw() # hide main window
    libmultitask.Thread(target=updatesearchwindow).start() # run updatesearchwindow without waiting for it to finish
    update = updatecheck()
    if not update == "no-connection-error":
        if not update == "internalerror":
            if version < update:
                print("Update available!")
                downloadupdate = askyesnocancel(installedlanguages[languageID][22], f"{str(version)} 🠒 {str(update)}\n\n{installedlanguages[languageID][23]}\n\n{installedlanguages[languageID][24]}\n{installedlanguages[languageID][52]}", hidegui=False) # Update available - Press 'Yes' to auto-update; 'No' to open website
                if downloadupdate == True:
                    print("User allowed auto-update.\nDownload new version...")
                    debdownload = (f"https://codeberg.org/Palace4Software/timesync/releases/download/v{update}/timesync-{update}.deb")
                    #download deb-package
                    try:
                        requests.get(debdownload)
                        subprocess.run(f'''wget -q {debdownload} -P {cache_path}/''', shell=True)
                        cancontinue = True
                    except:
                        cancontinue = False
                    #install deb-package
                    if cancontinue == True:
                            if not subprocess.run(f'''pkexec apt install {cache_path}/timesync-{update}.deb -y''', shell=True).returncode == 0:
                                showerror(installedlanguages[languageID][22], installedlanguages[languageID][31], hidegui=False) # wrong password or process aborted
                                print("Error - Wrong password or process aborted")
                            else:
                                showinfo(installedlanguages[languageID][22], installedlanguages[languageID][53] + "\n\n" + installedlanguages[languageID][34], hidegui=False) # update successful installed, you might need to restart the application
                            if not subprocess.run(f'''rm {cache_path}/timesync-*.deb*''', shell=True).returncode == 0:
                                print("Error while removing cache files!")
                    else:
                        print("Unknown (connection) error! Check internet connection, try download manually!")
                        showerror(installedlanguages[languageID][22] + " - " + installedlanguages[languageID][46], installedlanguages[languageID][50] + "\n\n" + installedlanguages[languageID][27] + "\n\n" + installedlanguages[languageID][51], hidegui=False) # unknown (connection) error, check connection, try download manually
                elif downloadupdate == False: 
                    print("User allowed opening browser.")
                    openweb("https://codeberg.org/Palace4Software/timesync/releases")
                else:
                    print("User denied auto-update and automatic browser open.")
            else:
                print("No updates available.")
                showinfo(installedlanguages[languageID][22], installedlanguages[languageID][25], hidegui=False) # currently no updates
        else:
            internalerror()
    else:
        print("Error - Couldn't check for updates! Check your internet connection.")
        messagebox.showerror(installedlanguages[languageID][46], installedlanguages[languageID][26] + "\n\n" + installedlanguages[languageID][27]) # connection error - error updatecheck, check internet connection
    destroyvar = True
    GUI.deiconify() # show main window again

def openweb(url):
    print("Open default browser with following content:", url)
    webbrowser.open(url)

print("General commands loaded.")

#syncronization commands
print("Load syncronization commands...")
def synctime(syncadress):
    #checks for "http(s)://" in the webadress and remove it
    if not syncadress[:7] == "http://" and not syncadress[:8] == "https://":
        syncadress = ("http://" + syncadress)
    #checks for internet connection or website exist
    try:
        requests.get(syncadress)
        cancontinue = True
    except:
        cancontinue = False
    #if internet connection exists - sync; else - sync not
    if cancontinue == True:
        print("Sync with " + syncadress + " ...")
        command = (f'''pkexec date -s "$(wget --method=HEAD -qSO- --max-redirect=0 {syncadress} 2>&1 | sed -n 's/^ *Date: *//p')"''')
        successfulchecker = subprocess.run((command), shell=True)
        print(successfulchecker)
        #check if sync was successful
        if successfulchecker.returncode == 0:
            showinfo(installedlanguages[languageID][28], installedlanguages[languageID][29]) # query successful
            print("The query was succesful")
        else:
            showerror(installedlanguages[languageID][30], installedlanguages[languageID][31]) # wrong password or process aborted
            print("Error - Wrong password or process aborted")
    else:
        showerror(installedlanguages[languageID][46], str(syncadress) + "\n\n" + installedlanguages[languageID][45]) # connection error - no internet connection or website does not exist
        print("Error - No internet connection or website does not exist")

def synccustom():
    #syncstart (see button command down)
    print("Initalize custom sync...")
    def syncstart():
        print("Sync with custom...")
        inputfieldentry = inputfield.get()
        if not inputfieldentry == installedlanguages[languageID][9]:
            GUI3.destroy()
            with open(config_path + "/lastcustom.cfg", "w") as lastcustom:
                print(inputfieldentry, file=lastcustom)
                print("", file=lastcustom)
            synctime(str(inputfieldentry))
        else:
            print("Sync canceled: Format error!")
            #hide custom-sync window, if error-message appears
            GUI3.withdraw()
            messagebox.showerror(installedlanguages[languageID][35], installedlanguages[languageID][36]) # format error - enter valid webadress
            GUI3.deiconify()
    #Initalisation second window
    print("Show custom sync window...")
    GUI3 = gui.Tk()
    GUI3.title(installedlanguages[languageID][8]) # "Custom sync"
    GUI3.minsize(width="300", height="125")
    GUI3.resizable(width=False, height=False)
    #Button and space
    print("Set buttons of custom sync window...")
    gui.Label(GUI3).pack(expand=True, fill="x")
    
    #create lastcustom.cfg if not exist and read from it
    configexist = os.path.isfile(config_path + "/lastcustom.cfg")
    if not configexist == True:
        with open(config_path + "/lastcustom.cfg", "w") as lastcustom:
            print("Enter webadress", file=lastcustom)
            print("", file=lastcustom)
    presetinput = open(config_path + "/lastcustom.cfg", "r")
    presetinput = presetinput.readline()
    presetinput = presetinput.replace("\n", "")
    if presetinput == "Enter webadress": # checks, if it's possible to preset "Enter webadress" into entryfield
        presetinput = str(installedlanguages[languageID][9]) # "Enter webadress"
    
    inputfield = gui.Entry(GUI3) #cannot be used with .pack(expand=True, fill="x")
    inputfield.pack(expand=True, fill="x")
    inputfield.insert(0, presetinput) #insert lastcustom.cfg (or default entry) into inputfield
    
    gui.Label(GUI3).pack(expand=True, fill="x")
    syncbutton = gui.Button(GUI3, text=(installedlanguages[languageID][10]), command=syncstart) # "Sync"
    syncbutton.pack()
    gui.Label(GUI3).pack(expand=True, fill="x")
    print("Buttons and spaces created.")
print("Syncronization commands loaded.")

#Initalisation of window
print("Initalize main program...")
GUI = gui.Tk()
GUI.title(installedlanguages[languageID][1]) # "Date/Time Synchronizer"
GUI.minsize(width="350", height="400")
GUI.resizable(width=False, height=False)

## Setup background and button
#First space
print("Set first space...")
spaceholder1 = gui.Label(GUI)
spaceholder1.pack(expand=True, fill="both")

#Headline (Title)
print("Set headline...")
headline = gui.Label(GUI, text=(installedlanguages[languageID][2]), font=("Liberation Serif", 13, "bold")) # "Date & Time Synchronzier"
headline.pack(expand=True, fill="x")

#logo and space between headline and buttons
print("Load logo...")
img_path = str(run_path + "/images/icon.png")
img = gui.PhotoImage(file=img_path).subsample(20)
imageline = gui.Label(GUI, image=img, compound="center")
imageline.pack(pady=15)

#Buttons
print("Initalize buttons...")
button1 = gui.Button(GUI, text=(installedlanguages[languageID][3]), command=lambda: synctime("https://debian.org"), bg="#cc0000", fg="#ffffff") # Sync debian
button1.pack()
button2 = gui.Button(GUI, text=(installedlanguages[languageID][4]), command=lambda: synctime("https://kernel.org"), bg="#1a1a00", fg="#ffffff") # Sync Linux
button2.pack()
button3 = gui.Button(GUI, text=(installedlanguages[languageID][5]), command=lambda: synctime("https://duckduckgo.com"), bg="#ff8000", fg="#000000") # Sync DuckDuckGo
button3.pack()
button4 = gui.Button(GUI, text=(installedlanguages[languageID][6]), command=lambda: synctime("https://time.google.com"), bg="#009933", fg="#000000") # Sync Google
button4.pack()
button5 = gui.Button(GUI, text=(installedlanguages[languageID][7]), command=synccustom, bg="#9999ff", fg="#000000") # Sync custom
button5.pack()

#Space between buttons and subbuttons
print("Initalize space between buttons and subbottons...")
spaceholder2 = gui.Label(GUI)
spaceholder2.pack(expand=True, fill="both")

#autoupdate
def flashbutton(): # flash backgroundcolor of aboutbutton to red and back to original
    if aboutbutton.cget("bg") == "#ff0000":
        aboutbutton.config(bg="#d3d3d3")
    else:
        aboutbutton.config(bg="#ff0000")
    aboutbutton.after(500, flashbutton)

if autoupdate == True:
    print("Autocheck for updates...")
    update = updatecheck()
    if not update == "no-connection-error":
        if not update == "internalerror":
            if version < update:
                update = True
                print("Update available!")
            else:
                update = False
        else:
            internalerror()
    else:
        update = False
else:
    update = False

#Buttons (Exit, About)
print("Set About button...")
aboutbutton = gui.Button(GUI, text=(installedlanguages[languageID][11]), command=lambda: aboutprogram(False)) # "About"
aboutbutton.pack(side="left")
if update == True:
    flashbutton()
    aboutbutton.config(command=lambda: aboutprogram(False, update=True))
print("Set Exit button...") #must be set before settingsbutton
exitbutton = gui.Button(GUI, text=(installedlanguages[languageID][13]), command=exitprogram) # "Exit"
exitbutton.pack(side="right")
print("Set Change language button...")
settingsbutton = gui.Button(GUI, text=(installedlanguages[languageID][12]), command=lambda: settings.main(config_path, cache_path, installedlanguages[languageID], installedlanguages)) # Settings
settingsbutton.pack(side="top")

#Space between subbuttons and end
print("Set update-information-space between subbuttons and end...")
if update == True:
    spaceholder3 = gui.Label(GUI, text=(" ↖  " + installedlanguages[languageID][23])) # "Update available"
    spaceholder3.pack(side="left", fill="x")
elif update == False:
    spaceholder3 = gui.Label(GUI)
else:
    print(f"Updateerror!\nupdatevariable: {update}")
    internalerror()

#load dummys
print("Load dummys...")
abouttextversion = gui.Label(GUI)
abouttextlicense = gui.Label(GUI)
licensebutton = gui.Button(GUI)
noticebutton = gui.Button(GUI)
tempspace = gui.Label(GUI)
backbutton = gui.Button(GUI)
# ! checkupdatebutton is now the aboutbutton !

print("Main window successfully loaded.")

GUI.mainloop()
print("Exit program...")
