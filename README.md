# timesync
Program to sync the time (graphical) with different (preset) servers and also a custom server function. Written for Debian GNU/Linux and systems they based on it. 

![Screenshot](screenshot.png)

## The application
### Why do I need this application?
Under the common Linux distributions, it is not possible to change the time synchronisation server easily (& graphically) on almost all desktops.
This app solves the problem and makes integrated synchronisation solutions superfluous.

Timesync also does instant synchronisation - not like NTP, where it waits until there is a certain deviation from the time on the server.

### How to install the application?
1. Download "timesync-{version}.deb" from the [Releases-section](https://codeberg.org/Palace4Software/timesync/releases).
2. Open the Downloads-folder with your file manager.
3. Double click the downloaded .deb -file and open it with your software manager (or graphical package installation program (like gdebi, ...))
4. Install the application.
    * _You have to replace "{version}" with the version you downloaded (for example "timesync-2.0.2.deb")._

### How to start the application?
* In your applications (e.g. GNOME application list, Start menu, or Mint menu), there should be a new application called "Time and Date Synchronization". You can launch it by clicking on it.
* If you need a debugging-enviroment, you can start the application from the terminal by typing ``` timesync --gui ```.
    * Just type ``` timesync ``` to see all available options.

### Which languages are available?
Currently are 18 languages available:
* Afrikaans
* Chinese (simplified)
* Danish
* Dutch
* English
* Finnish
* French
* German
* Italian
* Japanese
* Norwegian
* Polish
* Portuguese
* Russian
* Spanish
* Swedish
* Turkish
* Ukrainian

### How to use the application?
#### Preset server
1. Start the application by clicking on the entry in your menu.
2. Press on the server you'd like to sync your date and time with.
3. Enter your password (or the root password) and press enter.
4. Time and date should now be synced.

#### Custom server
1. Start the application by clicking on the entry in your menu.
2. Press on "Sync with custom".
3. Enter the server or website you'd like to sync with (e.g. opensource.org) and click the "Sync" button. You can also type in IP-Adresses. *(Not every website/server allows time synchronization! If it is not possible to sync, the time is automatically set to 0 o'clock.)*
4. Enter your password (or the root password) and press enter.
5. Time and date should now be synced.

#### Terminal mode
1. Start a terminal window.
2. Type ``` timesync --sync {server to sync with} ``` (for example: ``` timesync --sync debian.org ```)
3. Time and date should now be synced.


### Settings
You can change following settings:
- Language
- Automatic update check
- Automatic Syncronisation
   - Automatic timesync, where you can enter a web address and the time is synchronized with this address whenever the computer is started.
   - Regularly timesync (every 6 hours)


## Building (not recommended for non-developers)
### How to build the .deb -Package
**WARNING:**  You might need to install some dependencies to use the automatic script.
1. Download the timesync repository (see website below) and extract all the files. (Maybe you should download the source-code from the release-section, because the raw repository can contain unstable features and code.)
    * You can also download via git (can contain unstable features and code): ``` git clone https://codeberg.org/Palace4Software/timesync.git ```
2. Extract the downloaded source code and open the folder.
3. Run the "build-deb.sh" script.
4. Now you got a new folder, called "installer". Here you can find the builded .deb -file. Just run it to install the application.



## Compatiblity
* This program is written for Debian Linux or Linux distributions based on it (e.g. Ubuntu Linux, Linux Mint, ...).
* You need python3 and python3-tk (Tkinter) in version 3.10.0 (or higher) to run the program. Also the packages python3-platformdirs, python3-requests, pkexec, sudo, wget, sed, fonts-liberation2. These are downloaded automatically during installation.


## The timesync-project
This project moved from GitHub to CodeBerg at the end of 2023. If you are looking for older releases (≤ 2.0.4), visit the Timesync [GitHub-Archive](https://github.com/Palace4Software/timesync).

Furthermore:
* See https://codeberg.org/Palace4Software/timesync for updates and informations and source code
* See https://codeberg.org/Palace4Software/timesync/commits/branch/main for changelog
